<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use yii\imagine\Image;
use Imagine\Image\Box;


/**
 * Site controller
 */
class BaseController extends Controller
{
    public function beforeAction($action) {
        if($action->actionMethod!== "actionLogin") {
            if(!Yii::$app->user->isGuest) {
                if (!Yii::$app->user->identity->is_admin) {
                    Yii::$app->user->logout();
                }
            }
            else {
                return $this->redirect("/site/login");
            }
        }
        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

}
