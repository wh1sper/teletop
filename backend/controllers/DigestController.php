<?php

namespace backend\controllers;

use PHPUnit\Framework\Exception;
use Yii;
use common\models\Digest;
use common\models\Category;
use common\models\Channel;
use backend\models\DigestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DigestController implements the CRUD actions for Digest model.
 */
class DigestController extends BaseController
{

    public $enableCsrfValidation = false;

    /**
     * Lists all Digest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DigestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Digest model.
     * @param integer $id
     * @return mixed
     */
    public function actionCategory($id)
    {

        if(isset($_POST['channel'])) {
            $model = new Digest();
            $model->channel_id = $_POST['channel'];
            $model->category_id = $id;
            $model->pos = $model->getMaxPos() + 1;
            $model->saveUnique();
        }

        $category = Category::findOne($id);

        return $this->render('category', [
            'category' => $category,
            'channels' => Channel::find()->all(),
        ]);
    }

    /**
     * Creates a new Digest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Digest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Digest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Digest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $id = $_REQUEST['id'];
        $id = explode("-",$id);
        $category_id = $id[0];
        $channel_id = $id[1];

        $model = Digest::find()->where(['channel_id'=>$channel_id,'category_id'=>$category_id])->one();
        $model->delete();

        return $this->redirect(['/digest/category/'.$category_id]);
    }


    /**
     * Finds the Digest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Digest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Digest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
