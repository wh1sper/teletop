<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use yii\imagine\Image;
use Imagine\Image\Box;


/**
 * Site controller
 */
class SiteController extends BaseController
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpload()
    {
        header('Content-type: application/json');

        if($_FILES['file']['type'] == 'image/jpeg') $extension = '.jpg';
        if($_FILES['file']['type'] == 'image/png') $extension = '.png';

//        $user = User::findOne(Yii::$app->user->id);

        if(!isset($extension)) {
            $file['error'] = 'Данный тип файла не поддерживается';
        }
        else {
            $name = time();

            $file_upload_path = Yii::getAlias('@frontend').UPLOAD_PATH.$name.$extension;
            if (file_exists($file_upload_path)) {
                unlink($file_upload_path);
            }

            if (!@move_uploaded_file($_FILES['file']['tmp_name'], $file_upload_path)) {
                $file['error'] = "move_uploaded_file error to $file_upload_path";
            }
            else {
                $imageObj = Image::getImagine()->open($file_upload_path);
                $imageObj->resize($imageObj->getSize()->widen(400))->save($file_upload_path);
                Image::thumbnail($file_upload_path,200,200)->save(str_replace($extension,"_thumb".$extension,$file_upload_path));
            }

            $file['file_path'] = $file_upload_path;
            $file['file_name'] = "$name$extension";
            $file['file_url'] = WEB_UPLOAD_PATH.$name.$extension;

//            $user->userpic = $file['file_name'];
//            $user->save();

        }
        die(json_encode($file, JSON_UNESCAPED_UNICODE));

    }
}
