<?php

namespace backend\controllers;

use common\models\Sticker;
use Yii;
use common\models\Stickerpack;
use frontend\models\StickerpackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StickerpackController implements the CRUD actions for Stickerpack model.
 */
class StickerpackController extends BaseController
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stickerpack models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StickerpackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stickerpack model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(isset($_POST['image'])) {
            $sticker = new Sticker();
            $sticker->pack_id = $id;
            $sticker->image = $_POST['image'];
            $sticker->save();
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDelsticker($id) {
        $sticker = Sticker::findOne($id);
        $model = $sticker->pack;
        $sticker->delete();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionFeatured() {
        $id = $_POST['id'];
        $val = $_POST['val'];
        $model = $this->findModel($id);
        $model->featured = $val;
        $model->save();
    }

    /**
     * Creates a new Stickerpack model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stickerpack();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Stickerpack model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stickerpack model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Stickerpack model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stickerpack the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stickerpack::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
