<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Название',
                'format' => 'raw',
                'value' => function($model) {
                    return "<a href='/category/update/{$model->id}'>{$model->name}</a>";
                }
            ],
            [
                'label' => 'Каналы',
                'format' => 'raw',
                'value' => function($model) {
                    $digest = $model->digest;
                    $channels_count = is_object($digest) ? sizeof($digest->channels) : 0;
                    return "<a href='/digest/category/{$model->id}'>Выбор редакции ($channels_count)</a>";
                }
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{delete}' ]
        ],
    ]); ?>
</div>
