<div class="row channel" data-id="<?= $model->id ?>">
    <div class="col-xs-1 position"><?= ++$index ?></div>
    <div class="col-xs-1"><img class='channel-image' src="<?= $model->getImage(); ?>"/></div>
    <div class="col-xs-8">
        <b><?= $model->name ?></b> <i><?= $model->id ?></i>
        <div class="channel-descr"><?= $model->descr ?></div>
    </div>
    <div class="col-xs-1"><span class="views"><?= $model->members ?></span> </div>
    <div class="col-xs-1">
        <a href="/digest/delete?id=<?= $category_id ?>-<?= $model->id ?>" class="btn btn-danger">X</a>
    </div>
</div>
