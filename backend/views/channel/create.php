<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Channel */

$this->title = 'Добавить каналы';
$this->params['breadcrumbs'][] = ['label' => 'Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>(по одному в строке, начиная с <b>@</b>)</p>

    <form method="post" action="">
        <textarea style='height:400px; width:400px;' name="add_channels"></textarea>
        <br>
        <input type="submit" name="submit" value="Добавить" class="btn btn-success"/>
    </form>

</div>
