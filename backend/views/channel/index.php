<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каналы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p style="padding-bottom:30px;">
        <a type="button" href="/channel?ChannelSearch[checked]=0" class="btn btn-default">На модерацию</a>

    <?= Html::a('Добавить каналы', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function($model) {
                    return "<img style='height:100px;' src='".$model->getImage()."'/>";
                }
            ],
            'id',
            'name',
            'members',
            [
                'label' => 'Категория',
                'format' => 'raw',
                'value' => function ($model) {
                    $ret = '<select class="category_select" data-id="'.$model->id.'">';
                    $ret .= '<option value="0">Не определена</option>';
                    foreach(\common\models\Category::find()->all() as $cat) {
                        $selected = ($model->category_id == $cat->id) ? "selected" : '';
                        $ret .= '<option '.$selected .' value="' . $cat->id . '">' . $cat->name . '</option>';
                    }
                    $ret .= '</select>';
                    return $ret;
                }
            ],

            [
                'label' => 'Проверен',
                'format' => 'raw',
                'value' => function ($model) {
                    $selected = $model->checked ? "checked" : '';
                    return "<input type='checkbox' $selected data-id='{$model->id}' class='channel_check'/>";
                }
            ],

            // 'descr:ntext',
            // 'updated',
            ['class' => 'yii\grid\ActionColumn','template'=>'{delete}' ]
        ]]
    ); ?>
</div>
