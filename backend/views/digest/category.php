<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Digest */

$this->title = "Выбор редакции: ".$category->name;
$this->params['breadcrumbs'][] = ['label' => 'Digests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="digest-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row" style="padding-top:40px;">
        <form action="" method="post">
        <div class="col-xs-4">
            <select name="channel">
                <? foreach($channels as $channel): ?>
                    <option value="<?= $channel->id ?>"><?= $channel->id ?>: <?= $channel->name ?></option>
                <? endforeach; ?>
            </select>

        </div>
        <div class="col-xs-1">
            <input type="submit" name="submit" value="Добавить" class="btn btn-success"/>
        </div>
        </form>
    </div>

    <div class="digest" style="padding-top:40px;">
        <? if(sizeof($category->digest->channels)): ?>
            <? foreach($category->digest->channels as $key=>$channel): ?>
                <?= $this->render("/channel/_item",["index"=>$key,"category_id"=>$category->id,"model"=>$channel]); ?>
            <? endforeach; ?>
        <? endif; ?>
    </div>


</div>
