<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <div class="row" style="margin:0;">
            <label class="control-label" for="image">Обложка</label>
            <?= $form->field($model, 'image')->hiddenInput(['id'=>'image'])->label(false) ?>

            <div class="col-xs-3" style="padding-left:0;">
                <div class="form-group small_dropper" data-field="image">
                    <span class="upload-button"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-5">
                <img class="sticker" src="<?= $model->getImage(); ?>">
            </div>
        </div>
    </div>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'category')->dropDownList($model::$categories); ?>

    <?= $form->field($model, 'descr')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
