<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Название',
                'format' => 'raw',
                'value' => function($model) {
                    return "<a href='/game/update/{$model->id}'>{$model->name}</a>";
                }
            ],

            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function($model) {
                    return "<img style='height:100px;' src='".$model->getImage()."'/>";
                }
            ],

            [
                'label' => 'Выбор',
                'format' => 'raw',
                'value' => function ($model) {
                    $selected = $model->featured ? "checked" : '';
                    return "<input type='checkbox' $selected data-id='{$model->id}' class='game_featured'/>";
                }
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{delete}'],
        ],
    ]); ?>
</div>
