<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Stickerpack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stickerpack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <div class="form-group">
        <div class="row" style="margin:0;">
            <label class="control-label" for="image">Обложка</label>
            <?= $form->field($model, 'image')->hiddenInput(['id'=>'image'])->label(false) ?>

            <div class="col-xs-3" style="padding-left:0;">
                <div class="form-group small_dropper" data-field="image">
                    <span class="upload-button"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <img class="sticker" src="<?= $model->getImage(); ?>">
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
