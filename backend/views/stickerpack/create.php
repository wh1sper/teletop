<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Stickerpack */

$this->title = 'Добавить стикерпак';
$this->params['breadcrumbs'][] = ['label' => 'Стикерпаки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 style="margin-bottom:30px;"><?= Html::encode($this->title) ?></h1>

<div class="stickerpack-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
