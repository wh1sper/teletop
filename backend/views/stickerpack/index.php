<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StickerpackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Стикерпаки';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickerpack-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function($model) {
                    return "<img style='height:100px;' src='".$model->getImage()."'/>";
                }
            ],


            [
                'label' => 'Название',
                'format' => 'raw',
                'value' => function($model) {
                    return "<a href='/stickerpack/update/{$model->id}'>{$model->name}</a>";
                }
            ],

            [
                'label' => 'Стикеры',
                'format' => 'raw',
                'value' => function($model) {
                    $stickers_count = sizeof($model->stickers);
                    return "<a href='/stickerpack/{$model->id}'>Редактировать ($stickers_count)</a>";
                }
            ],

            [
                'label' => 'Выбор',
                'format' => 'raw',
                'value' => function ($model) {
                    $selected = $model->featured ? "checked" : '';
                    return "<input type='checkbox' $selected data-id='{$model->id}' class='stickerpack_featured'/>";
                }
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{delete}'],
        ],
    ]); ?>
</div>
