<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Stickerpack */

$this->title = "Стикерпак: ".$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Stickerpacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickerpack-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="form-group">
        <form action="" method="post">
            <input type="hidden" name="image" id="image"/>
            <div class="row" style="margin:0;">
                <div class="col-xs-3" style="padding-left:0;">
                    <div class="form-group small_dropper" data-field="image">
                        <span class="upload-button"></span>
                    </div>
                </div>

                <div class="col-xs-3">

                </div>

                <img class="sticker" style="display:none; width:200px;" src="">

            </div>

            <div class="row">
                <div class="col-xs-3">
                    <input type="submit" class="btn btn-success" value="Добавить"/>
                </div>
            </div>


        </form>
    </div>

    <div class="row" style="padding-top: 50px; padding-bottom:100px;">
        <? foreach($model->stickers as $sticker): ?>
            <div class="sticker">
                <?
                echo branchonline\lightbox\Lightbox::widget([
                    'files' => [
                        [
                            'thumb' => $sticker->getImage(),
                            'original' => $sticker->getImage(),
                        ],
                    ]
                ]);
                ?>
                <a href="/stickerpack/delsticker/<?= $sticker->id ?>">Удалить</a>

            </div>

        <? endforeach; ?>
    </div>

</div>
