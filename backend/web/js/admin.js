$(document).on('change','.category_select',function(data) {
    $.post("/channel/category", { channel: $(this).data('id'), category: $(this).val() });
});

$(document).on('change','.stickerpack_featured',function(data) {
    var val;
    if($(this).is(":checked")) val = 1;
    else val = 0;
    $.post("/stickerpack/featured", { id: $(this).data('id'), val: val });
});

$(document).on('change','.game_featured',function(data) {
    var val;
    if($(this).is(":checked")) val = 1;
    else val = 0;
    $.post("/game/featured", { id: $(this).data('id'), val: val });
});


$(document).on('change','.channel_check',function(data) {
    var val;
    if($(this).is(":checked")) val = 1;
    else val = 0;
    $.post("/channel/check", { id: $(this).data('id'), val: val });
});