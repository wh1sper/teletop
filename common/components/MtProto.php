<?php
namespace common\components;

use Yii;
use yii\base\Component;
use Dotenv;

class MtProto extends Component
{

    public static $instance = null;

    public function getInstance()
    {
        if(self::$instance == null) {

            if (file_exists(APP_PATH.'/.env')) {
                echo 'Loading .env...' . PHP_EOL;
               // $dotenv = new Dotenv\Dotenv(getcwd());
                $dotenv = new Dotenv\Dotenv(APP_PATH);
                $dotenv->load();
            }

            echo 'Loading settings...' . PHP_EOL;
            $settings = json_decode(getenv('MTPROTO_SETTINGS'), true) ?: [];

            echo 'Deserializing MadelineProto from session.madeline...' . PHP_EOL;

            $MadelineProto = false;

            try {
                $MadelineProto = \danog\MadelineProto\Serialization::deserialize(APP_PATH.'/session.madeline');
            } catch (\danog\MadelineProto\Exception $e) {
                var_dump($e->getMessage());
            }

            if ($MadelineProto === false) {

                $MadelineProto = new \danog\MadelineProto\API($settings);

                $number = defined('TG_TEST') ? getenv('TEST_NUMBER'): getenv('MTPROTO_NUMBER');
                $sentCode = $MadelineProto->phone_login($number, true);

                echo 'Enter the code you received: ';
                $code = '';
                for ($x = 0; $x < $sentCode['type']['length']; $x++) {
                    $code .= fgetc(STDIN);
                }
                $MadelineProto->complete_phone_login($code);

            }

            self::$instance = $MadelineProto;
        }

        return self::$instance;

    }

    public function serialize() {
        $MadelineProto = $this->getInstance();
        echo 'Serializing MadelineProto to session.madeline...' . PHP_EOL;
        echo 'Wrote ' . \danog\MadelineProto\Serialization::serialize('session.madeline', $MadelineProto) . ' bytes' . PHP_EOL;
        echo 'Size of MadelineProto instance is ' . strlen(serialize($MadelineProto)) . ' bytes' . PHP_EOL;

    }

    public function getFirstUpdate() {
        $MadelineProto = $this->getInstance();
        $updates = (array) $MadelineProto->API->updates;
        $keys = array_keys($updates);
        return sizeof($updates) ? reset($keys) : 0;
    }

}