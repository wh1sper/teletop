<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "channels".
 *
 * @property integer $id
 * @property string $username
 * @property integer $joined
 * @property string $image
 * @property string $name
 * @property integer $members
 * @property integer $tg_id
 * @property string $descr
 */
class Channel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['joined', 'members','updated','checked'], 'integer'],
            [['descr'], 'string'],
            [['id', 'name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 300],
        ];
    }

    public static function find()
    {
        return new ChannelQuery(get_called_class());
    }

    public function getMessages() {
        return $this->hasMany(Message::className(), ['channel_id' => 'id']);
    }

    public function getLentas() {
        return $this->hasMany(Lenta::className(), ['channel_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Канал',
            'joined' => 'Joined',
            'image' => 'Image',
            'name' => 'Название',
            'members' => 'Участники',
            'descr' => 'Описание',
        ];
    }

    public static function reset() {
        Channel::updateAll(['updated'=>0], ['updated'=>1]);
    }

    public function getImage() {
        return WEB_DOWNLOAD_PATH.$this->image;
    }

    public function delete() {
        if(sizeof($this->messages)) {
            foreach ($this->messages as $msg) {
                $msg->delete();
            }
        }
        if(sizeof($this->lentas)) {
            foreach ($this->lentas as $lenta) {
                $lenta->delete();
            }
        }
        parent::delete();
    }

}
