<?php

namespace common\models;

use Yii;

class ChannelQuery extends \yii\db\ActiveQuery
{

    public function unjoined()
    {
        $this->andWhere(['joined' => 0]);
        return $this;
    }

    public function joined()
    {
        $this->andWhere(['joined' => 1]);
        return $this;
    }

    public function unupdated()
    {
        $this->andWhere(['updated' => 0]);
        return $this;
    }

    public function category($id) {
        $this->andWhere(['category_id' => $id]);
        return $this;
    }

    public function checked()
    {
        $this->andWhere(['checked' => 1]);
        return $this;
    }

    public function unchecked()
    {
        $this->andWhere(['checked' => 0]);
        return $this;
    }



}