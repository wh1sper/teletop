<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "digest".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $channel_id
 * @property integer $pos
 */
class Digest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'digest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'pos'], 'integer'],
            ['channel_id','safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'channel_id' => 'Channel ID',
            'pos' => 'Pos',
        ];
    }

    public function saveUnique() {
        $check = $this->find()->where(['channel_id'=>$this->channel_id,'category_id'=>$this->category_id])->count();
        if(!$check) return $this->save();
        else return false;
    }

    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getChannels() {
        
        $connection = Yii::$app->getDb();
        $sql = "SELECT channels.id FROM channels INNER JOIN digest ON digest.channel_id=channels.id WHERE digest.category_id='{$this->category_id}' ORDER BY digest.pos";
        $model = $connection->createCommand($sql);
        $result = $model->queryAll();

        $models = [];
        if($result) {
            foreach ($result as $res) {
                $models[] = Channel::findOne($res['id']);
            }
        }
        return $models;

    }

    public function getMaxPos() {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT max(pos) FROM digest WHERE category_id='".$this->category_id."'");
        $result = $command->queryScalar();
        return $result;
    }


}
