<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $descr
 * @property integer $date
 * @property string $category
 * @property string $link
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    public static $categories = [
        'arcade' => 'Аркады',
        'platform' => 'Платформеры',
        'riddle' => 'Головоломки',
        'sport' => 'Спорт',
        'racing' => 'Гонки',
        'cards' => 'Азартные',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descr', 'category'], 'string'],
            [['name', 'link'], 'string', 'max' => 1000],
            [['image'], 'string', 'max' => 100],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Игра',
            'image' => 'Лого',
            'descr' => 'Описание',
            'category' => 'Жанр',
            'link' => 'Ссылка',
        ];
    }

    public function getImage() {
        return empty($this->image) ? FRONT_WEB."/img/sticker.png" : WEB_UPLOAD_PATH.$this->image;
    }
}
