<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lenta".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $channel_id
 */
class Lenta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lenta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['channel_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'channel_id' => 'Channel ID',
        ];
    }


    public function getChannels() {
        return $this->hasMany(Category::className(), ['id' => 'channel_id']);
    }

    public function getChannel() {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    public static function channel($channel_id) {
        $user_id = Yii::$app->user->id;
        $exist = self::isUserSubscribed($channel_id);
        if($exist) {
            self::del($channel_id,$user_id);
            $ret = 0;
        }
        else {
            self::add($channel_id,$user_id);
            $ret = 1;
        }
        return $ret;
    }

    public static function add($channel_id,$user_id) {
        $model = new Lenta;
        $model->user_id = $user_id;
        $model->channel_id = $channel_id;
        $model->save();
    }

    public static function del($channel_id,$user_id) {
        Lenta::find()->where(['channel_id'=>$channel_id,'user_id'=>$user_id])->one()->delete();
    }

    public static function isUserSubscribed($channel_id) {
        $user_id = Yii::$app->user->id;
        return Lenta::find()->where(['channel_id'=>$channel_id,'user_id'=>$user_id])->count();
    }


}
