<?php

namespace common\models;

use Yii;
use yii\console\Exception;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $channel_id
 * @property string $hash
 * @property string $text
 * @property integer $date
 * @property integer $views
 * @property integer $tg_id
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'tg_id'], 'integer'],
            [['channel_id', 'text'], 'string'],
            [['hash'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_id' => 'Channel ID',
            'hash' => 'Hash',
            'text' => 'Text',
            'date' => 'Date',
            'tg_id' => 'Tg ID',
        ];
    }


    public function getMedia() {
        return $this->hasOne(Media::className(), ['id' => 'media_id']);
    }

    public function getUpdates() {
        return $this->hasMany(Update::className(), ['message_id' => 'id']);
    }

    public function getChannel() {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    public static function checkExists($hash) {
        return Message::findOne(['hash' => $hash]);
    }

    public function getViews() {
        $updates = $this->updates;
        return end($updates)->views;
    }

    public function getDate() {
        return date("d/m/Y H:i",$this->date);
    }

    public function getContent() {
        if($this->media) {
            return $this->media->type;
        }
        else return "text";
    }


    public static function find()
    {
        return new MessageQuery(get_called_class());
    }

    public static function findByChannel($channel_id,$message_id) {
        return self::find()->where(['channel_id'=>$channel_id,'tg_id'=>$message_id])->one();
    }

    public static function add($upd) {
        $channels = Channel::find()->checked()->all();
        foreach($channels as $ch) {
            $ch_data[$ch->tg_id] = $ch->id;
        }

        $hash = md5($upd['to_id']['channel_id'].$upd['id']);
        if($exists = static::checkExists($hash)) {
            // добавляем апдейт
            Update::add($exists->id,$upd['views']);
        }
        else {

            $message = new Message();
            $message->tg_id = $upd['id'];
            $message->date = $upd['date'];
            $message->channel_id = $ch_data[$upd['to_id']['channel_id']];
            $message->hash = $hash;
            $message->text = $upd['message'];
            if (isset($upd['media']) && sizeof($upd['media'])) {
                if ($upd['media']['_'] == 'messageMediaPhoto') {
                    sleep(3);
                    $media = new Media;
                    $media->type = "img";
                    $file = Yii::$app->MtProto->getInstance()->download_to_dir($upd['media'], DOWNLOAD_PATH);
                    if(isset($upd['media']['caption'])) $media->caption = $upd['media']['caption'];
                    $media->src = str_replace(DOWNLOAD_PATH, '', $file);
                    $media->save();
                    $message->media_id = $media->id;
                }
                if ($upd['media']['_'] == 'messageMediaWebPage') {
                    $media = new Media;
                    $media->type = 'webpage';
                    $media->caption = '';
                    if(isset($upd['media']['webpage']['title'])) {
                        $media->caption .= "<h3>".$upd['media']['webpage']['title']."</h3>";
                    }
                    if(isset($upd['media']['webpage']['description'])) {
                        $media->caption .= $upd['media']['webpage']['description'];
                    }
                    if($upd['media']['webpage']['_'] !== 'webPageEmpty') {
                        $media->src = $upd['media']['webpage']['url'];
                    }
                    $media->save();
                    $message->media_id = $media->id;
                }

                if(!isset($media) && strlen($message->text) < 2)  {
                    return false;
                }
            }

            if ($message->save()) {
                Update::add($message->id, $upd['views']);
            }
        }
    }


    public static function top($type) {
        $top = [];

        if($type == 'hourly') {
            $hours = 6;
            $method = "getHourly";
        }
        else {
            $hours = 24;
            $method = "getDaily";
        }

        $messages = Message::find()->hours($hours)->all();
        foreach($messages as $message) {
            $updates = Update::$method($message->id);

            if(sizeof($message->updates) == 1) {
                $update = end($updates);
                if($update) $views = end($updates)->views;
            }
            elseif (sizeof($updates) > 1) {
                $first = reset($updates)->views;
                $last = end($updates)->views;
                $views = $last - $first;
            }
            else $views = 0;
            if(isset($views)) $top[$views][] = $message;
        }

        ksort($top);
        $top = array_reverse($top,true);

        return $top;
    }

    public function delete() {
        if($this->media) {
            $this->media->delete();
        }
        parent::delete();
    }


}
