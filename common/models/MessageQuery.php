<?php

namespace common\models;

use Faker\Provider\zh_TW\DateTime;
use Yii;

class MessageQuery extends \yii\db\ActiveQuery
{

    public function hours($hours)
    {
        $dt = new \DateTime();
        $to = $dt->getTimestamp();
        $dt->modify("-$hours hour");
        $from = $dt->getTimestamp();
        $this->andWhere(['between', 'date', $from, $to]);
        return $this;
    }


}