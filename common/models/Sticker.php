<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stickers".
 *
 * @property integer $id
 * @property integer $pack_id
 * @property string $image
 */
class Sticker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stickers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pack_id'], 'integer'],
            [['image'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pack_id' => 'Pack ID',
            'image' => 'Image',
        ];
    }

    public function getImage() {
        return empty($this->image) ? FRONT_WEB."/img/sticker.png" : WEB_UPLOAD_PATH.$this->image;
    }

    public function getPack() {
        return $this->hasOne(Stickerpack::className(), ['id' => 'pack_id']);
    }
}
