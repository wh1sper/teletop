<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stickerpacks".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date
 * @property string $image
 */
class Stickerpack extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stickerpacks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'link', 'image'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'link' => 'Ссылка',
            'image' => 'Обложка',
        ];
    }

    public function getStickers() {
        return $this->hasMany(Sticker::className(), ['pack_id' => 'id']);
    }

    public function getImage() {
        return empty($this->image) ? FRONT_WEB."/img/sticker.png" : WEB_UPLOAD_PATH.$this->image;
    }


}
