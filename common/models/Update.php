<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "updates".
 *
 * @property integer $id
 * @property integer $time
 * @property integer $message_id
 * @property integer $views
 */
class Update extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'updates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'message_id', 'views'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'message_id' => 'Message ID',
            'views' => 'Views',
        ];
    }

    public function getMessage() {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }


    public static function find()
    {
        return new UpdateQuery(get_called_class());
    }

    public static function add($message_id,$views) {
        $update = new Update;
        $update->time = time();
        $update->message_id = $message_id;
        $update->views = $views;
        $update->save();
    }

    public static function getHourly($message_id) {
        return Update::find()->where(['message_id'=>$message_id])->hourly()->all();
    }

    public static function getDaily($message_id) {
        return Update::find()->where(['message_id'=>$message_id])->daily()->all();
    }

}
