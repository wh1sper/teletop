<?php

namespace common\models;

use Yii;

class UpdateQuery extends \yii\db\ActiveQuery
{

    public function hourly()
    {
        $dt = new \DateTime();
        $to = $dt->getTimestamp();
        $dt->modify("-1 hour");
        $from = $dt->getTimestamp();
        $this->andWhere(['between', 'time', $from, $to]);
        return $this;
    }

    public function daily() {
        $dt = new \DateTime();
        $to = $dt->getTimestamp();
        $dt->modify("-24 hour");
        $from = $dt->getTimestamp();
        $this->andWhere(['between', 'time', $from, $to]);
        return $this;
    }


}