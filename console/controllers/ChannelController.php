<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Channel;
use Yii;

class ChannelController extends Controller
{

    public function actionJoin()
    {

        $channels = Channel::find()->checked()->unjoined()->all();

        if(sizeof($channels)) {
            foreach ($channels as $ch) {
                if (!$ch->joined) {
                    Yii::$app->MtProto->getInstance()->channels->joinChannel(['channel' => $ch->id]);
                    $info = Yii::$app->MtProto->getInstance()->get_full_info($ch->id);
                    $ch->tg_id = $info['Chat']['id'];
                    $ch->members = $info['full']['participants_count'];
                    $ch->descr = $info['full']['about'];
                    if(empty($ch->image) && isset($info['full']['chat_photo']['size'])) {
                        $file = Yii::$app->MtProto->getInstance()->download_to_dir($info['full']['chat_photo']['sizes'][0], DOWNLOAD_PATH);
                        $ch->image = str_replace(DOWNLOAD_PATH, '', $file);
                    }
                    $ch->name = $info['Chat']['title'];
                    $ch->joined = 1;
                    $ch->save();
                }
            }
        }

    }

    public function actionUpdate()
    {
        $ch = Channel::find()->checked()->unupdated()->one();
        if($ch) {
            $info = Yii::$app->MtProto->getInstance()->get_full_info($ch->id);
            $ch->tg_id = $info['Chat']['id'];
            $ch->members = $info['full']['participants_count'];
            $ch->descr = $info['full']['about'];
            if(empty($ch->image) && isset($info['full']['chat_photo']['sizes'])) {
                $file = Yii::$app->MtProto->getInstance()->download_to_dir($info['full']['chat_photo']['sizes'][0], DOWNLOAD_PATH);
                $ch->image = str_replace(DOWNLOAD_PATH, '', $file);
            }
            $ch->name = $info['Chat']['title'];
            $ch->updated = 1;
            $ch->save();
        }
    }

    public function actionRest() {
        Channel::reset();
    }

}
