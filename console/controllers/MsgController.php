<?php
namespace console\controllers;

use PHPUnit\Framework\Exception;
use yii\console\Controller;
use common\models\Message;
use common\models\Update;
use common\models\Channel;
use Yii;

class MsgController extends Controller
{

    public function actionHistory() {
        return false;
        $channels = Channel::find()->joined()->unupdated()->limit(10)->all();
        //$channels = Channel::find()->where(['username'=>'@meduzalive'])->all();
        if(sizeof($channels)) {
            foreach($channels as $channel) {
                $messages = Yii::$app->MtProto->getInstance()->messages->getHistory(['peer' => $channel->username, 'offset_id' => 0, 'offset_date' => 0, 'add_offset' => 0, 'limit' => 3, 'max_id' => 0, 'min_id' => 0]);
                if (sizeof($messages['messages'])) {
                    foreach ($messages['messages'] as $msg) {
                       Message::add($msg);
                       sleep(2);
                    }
                }
                $channel->updated = 1;
                $channel->save();
            }
        }
        else {
            Channel::reset();
        }
    }


    public function actionUpdate()
    {

        $stored_offset = file_get_contents(APP_PATH."/update_id.txt");
        $current_offset = Yii::$app->MtProto->getFirstUpdate();
        if($stored_offset != $current_offset) file_put_contents(APP_PATH."/update_id.txt",$current_offset);

        $offset = file_get_contents(APP_PATH."/update_id.txt");

//
//        $updates = Yii::$app->MtProto->getInstance()->API->updates;
//        print_r($updates);
//        die();
//

        $updates = Yii::$app->MtProto->getInstance()->API->get_updates(['offset' => $offset, 'limit' => 1000, 'timeout' => 1]);

        if (sizeof($updates)) {
            foreach ($updates as $upd) {
                if($upd['update']['_'] == 'updateNewChannelMessage') {
                    try {
                        Message::add($upd['update']['message']);
                    }
                    catch (\danog\MadelineProto\Exception $e) {
                        print_r($upd);
                        echo $e->getMessage();
                    }
                }
                $offset = $upd['update_id'];
            }
            file_put_contents(APP_PATH."/update_id.txt",$offset);
        }

        Yii::$app->MtProto->serialize();
    }

    public function actionViews() {

        $messages = Message::find()->hours(24)->all();
        $channels = [];
        foreach($messages as $msg) {
            $channels[$msg->channel_id][] = $msg->tg_id;
        }

        foreach($channels as $channel=>$messages) {
            $ch = Channel::findOne($channel);
            $views = Yii::$app->MtProto->getInstance()->messages->getMessagesViews(['peer' => $ch->id, 'id' => $messages, 'increment' => true]);
            foreach($messages as $k=>$msg) {
                $message_object = Message::findByChannel($channel,$msg);
                $update = new Update;
                $update->time = time();
                $update->message_id = $message_object->id;
                $update->views = $views[$k];
                $update->save();
            }
        }

        Yii::$app->MtProto->serialize();
    }
}


?>
