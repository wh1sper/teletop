<?php

namespace frontend\controllers;
use common\models\Lenta;
use Yii;

class BaseController extends \yii\web\Controller {
    public function beforeAction($action) {
        $lenta = $lenta = Lenta::find()->where(['user_id'=>Yii::$app->user->id])->all();
        $channels = [];
        if(sizeof($lenta)) {
            foreach($lenta as $ch) {
                if($ch->channel) {
                    if ($ch->channel->checked) {
                        $channels[] = $ch->channel_id;
                    }
                }
            }
        }
        if(!sizeof($channels)) $empty_lenta = 1;
        $this->view->params['lenta'] = $lenta;
        $this->view->params['channels'] = $channels;
        $this->view->params['empty_lenta'] = isset($empty_lenta) ? $empty_lenta : 0;
        return true;
    }
}

?>