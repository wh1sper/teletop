<?php

namespace frontend\controllers;

use Yii;
use common\models\Category;
use frontend\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CategoryController extends BaseController
{

    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($slug)
    {

        $categories = Category::find()->all();
        $model = $this->findModel($slug);
        if($model) {
            $channels = \common\models\Channel::find()->category($model->id)->all();
        }
        return $this->render('view', [
            'categories' => $categories,
            'model' => $model,
            'channels' => sizeof($channels) ? $channels : [],
        ]);
    }

    public function actionDigest($slug) {
        $categories = Category::find()->all();

        return $this->render('digest', [
            'categories' => $categories,
            'model' => $this->findModel($slug),
        ]);
    }

    protected function findModel($slug)
    {
        if (($model = Category::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
