<?php

namespace frontend\controllers;

use Rollbar\LevelTest;
use Yii;
use common\models\Channel;
use common\models\Lenta;
use common\models\User;
use frontend\models\ChannelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Category;

/**
 * ChannelController implements the CRUD actions for Channel model.
 */
class ChannelController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionAdd() {
        $id = $_POST['id'];
        $id = preg_replace("/[^a-zA-Z0-9-_]/", "", $id);
        $id = "@".$id;

        if(strlen($id) < 3)  {
            $status = 0;
            $added = 0;
        }
        else {
            $user_id = Yii::$app->user->id;

            $lenta = Lenta::find()->where(['user_id'=>$user_id])->all();
            if(sizeof($lenta)) {
                foreach($lenta as $channel) {
                    if($id == $channel->channel_id) return json_encode(['status'=>0,'added'=>0]);
                }
            }

            $channel = Channel::findOne($id);
            $added = 0;
            if (!$channel) {
                $ch = new Channel();
                $ch->id = $id;
                $ch->joined = 0;
                $ch->updated = 1;
                $ch->save();
                $added = 1;
            }

            $lenta = new Lenta;
            $lenta->user_id = $user_id;
            $lenta->channel_id = $id;
            if($lenta->save()) $status = 1;
        }
        return json_encode(['success'=>$status,'added'=>$added]);
    }

    /**
     * Lists all Channel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $categories = Category::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'categories' => $categories,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Channel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionLenta() {
        $channel_id = $_REQUEST['channel_id'];
        $status = Lenta::channel($channel_id);
        echo $status;
    }


    /**
     * Finds the Channel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Channel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Channel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
