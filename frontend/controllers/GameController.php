<?php

namespace frontend\controllers;

use Yii;
use common\models\game;
use frontend\models\GameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class GameController extends BaseController
{

    public function actionIndex()
    {
        $searchModel = new GameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'category' => 'all',
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCategory($category) {
        $searchModel = new GameSearch();
        $dataProvider = $searchModel->search(['category'=>$category]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionDigest()
    {
        $searchModel = new GameSearch();
        $params['featured'] = 1;
        $dataProvider = $searchModel->search($params);

        return $this->render('digest', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
