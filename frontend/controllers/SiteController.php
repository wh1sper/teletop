<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\MessageSearch;
use rmrevin\yii\ulogin\AuthAction;


/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */

    public function beforeAction($action)
    {
        if ($this->action->id == 'ulogin-auth')
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'ulogin-auth' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'uloginSuccessCallback'],
                'errorCallback' => function($data){
                    \Yii::error($data['error']);
                },
            ]
        ];
    }


    public function uloginSuccessCallback($attributes)
    {
        if(isset($attributes['email'])) {

            $login_model = new LoginForm();
            $login_model->email = $attributes['email'];
            $login_model->password = 'social';

            if($login_model->getUser()) {
                if ($login_model->login()) {
                    return $this->goHome();
                }

                return $this->render('signup', [
                    'model' => $login_model,
                ]);
            }

            $model = new SignupForm();
            $model->email = $attributes['email'];
            if ($user = $model->signup($attributes['network'])) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {

        if(!Yii::$app->user->isGuest) {
            $searchModel = new MessageSearch();
            $dataProvider = $searchModel->search(['channel_id' => $this->view->params['channels']]);

            return $this->render('/lenta/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'empty_lenta' => $this->view->params['empty_lenta'],
                'lenta' => $this->view->params['lenta'],
                'channels' => $this->view->params['channels']
            ]);
        }
        else {
            if(isset($_REQUEST['Signup'])) {
                $model = new SignupForm();
                $model->email = $_POST['Signup']['email'];
                $model->password = $_POST['Signup']['password'];

                if ($user = $model->signup('site')) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->goHome();
                    }
                }
                else {
                    $this->layout = "login";
                    return $this->render('login', [
                        'model' => $model,
                        'show_reg' => true,
                    ]);
                }
            } else {
                $model = new LoginForm();
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    return $this->goHome();
                } else {
                    $this->layout = "login";
                    return $this->render('login', [
                        'model' => $model
                    ]);
                }
            }
        }
    }

    public function actionGet() {
        return $this->render('get');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup('site')) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
