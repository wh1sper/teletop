<?php

namespace frontend\controllers;

use Yii;
use common\models\Stickerpack;
use frontend\models\StickerpackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * StickerpackController implements the CRUD actions for Stickerpack model.
 */
class StickerpackController extends BaseController
{

    public function actionIndex()
    {
        $searchModel = new StickerpackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDigest()
    {
        $searchModel = new StickerpackSearch();
        $params['featured'] = 1;
        $dataProvider = $searchModel->search($params);

        return $this->render('digest', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Stickerpack::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
