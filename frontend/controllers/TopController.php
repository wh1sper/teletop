<?php

namespace frontend\controllers;

use Yii;
use common\models\Message;

class TopController extends BaseController
{
    public function actionIndex()
    {
        $hourly = Message::top("hourly");
        $daily = Message::top("daily");
        return $this->render('index',['hourly' => $hourly,'daily' => $daily]);
    }

}
