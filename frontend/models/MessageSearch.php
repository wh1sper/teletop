<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Message;

/**
 * MessageSearch represents the model behind the search form about `common\models\Message`.
 */
class MessageSearch extends Message
{


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        if(!sizeof($params['channel_id'])) $params['channel_id'] = 0;

        $query->andFilterWhere([
            'channel_id' => $params['channel_id'],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->addOrderBy("date DESC");

        $dataProvider->pagination = ['pageSize' => 50];

        return $dataProvider;
    }
}
