<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Channel */

$this->title = $model->name;

?>


<div class="teletop_main">

    <?= $this->render("widget",['active'=>$model->slug,'categories'=>$categories]); ?>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="teletop_main_chanels">

        <div class="teletop_main_list_category" style="margin-bottom:50px;">
            <ul class="clearfix">
                <li class="active"><a href="/category/<?= $model->slug ?>">Популярные</a></li>
                <li><a href="/digest/<?= $model->slug ?>">Выбор редакции</a></li>
            </ul>
        </div>

        <ol>

            <? if($channels): ?>
                <? foreach($channels as $key=>$channel): ?>
                    <?= $this->render("/channel/_item",["index"=>$key,"model"=>$channel]); ?>
                <? endforeach; ?>
            <? endif; ?>

        </ol>
    </div>
</div>
