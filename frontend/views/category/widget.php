<div class="teletop_main_category">
    <ul>
        <li <? if($active == 'all'):?> class="active" <? endif; ?>><a href="/channels">Все</a></li>
        <? if(sizeof($categories)): ?>
            <? foreach($categories as $category): ?>
                <li <? if($active == $category->slug): ?> class="active" <? endif; ?>><a href="/category/<?= $category->slug ?>"><?= $category->name ?></a></li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>