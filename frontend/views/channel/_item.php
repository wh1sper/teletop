
<li>
    <table>
        <tr>
            <td class="td_channel_icon">
                <div class="channel_icon" style="background-image: url(<?= $model->getImage(); ?>"></div>
            </td>
            <td class="td_channel_text">
                <div class="channel_text">
                    <div class="channel_name"><?= $model->name ?> <b><?= $model->id ?></b></div>
                    <div class="channel_about"><?= $model->descr ?></div>
                </div>
            </td>
            <td class="td_channel_subscribers"><?= $model->members ?></td>
            <td class="td_channel_subscriber">

            <button data-id="<?= $model->id ?>" class="add-channel button <? if(common\models\Lenta::isUserSubscribed($model->id)):?>button_red<?else:?>button_green<?endif;?>"><? if(common\models\Lenta::isUserSubscribed($model->id)): ?>Отписаться<?else:?>Подписаться<?endif;?></button></td>
        </tr>
    </table>
</li>
