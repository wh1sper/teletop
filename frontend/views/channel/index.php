<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Channels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teletop_main">

    <?= $this->render("/category/widget",['active'=>'all','categories'=>$categories]); ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="teletop_main_chanels">
    <ol>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
//        'itemOptions' => ['class' => 'item'],
            // 'options' => ['id' => $type,'class' => 'row list-view '.mb_strtolower(Post::$types[$type]['eng'])],
//        'itemOptions' => ['class' => 'item '.$class],
//        'itemView' => function ($model, $key, $index, $widget) {
//            return $this->render('list/'.$model->type.'.php', ['model' => $model, 'index' => $index]);
//       },
            'itemView' => '_item',
            'itemOptions' => [
                'tag' => false
            ],
            'pager' => ['class' => \kop\y2sp\ScrollPager::className(),
                'triggerTemplate' => '<div class="show-more text-center"><a href="#lastnews"></a><div>{text}</div>'],
            'layout'=>"{items} {pager}",
        ]); ?>
    </ol>
    </div>
</div>

