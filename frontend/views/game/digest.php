<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игры';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="teletop_main">
    <div class="teletop_main_list teletop_main_list_with_category">
        <div class="teletop_main_list_category">
            <ul class="clearfix">
                <li><a href="/game">Популярные</a></li>
                <li class="active"><a href="/game/digest">Выбор редакции</a></li>
            </ul>
        </div>
        <div class="teletop_main_lists">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
//        'itemOptions' => ['class' => 'item'],
                // 'options' => ['id' => $type,'class' => 'row list-view '.mb_strtolower(Post::$types[$type]['eng'])],
//        'itemOptions' => ['class' => 'item '.$class],
//        'itemView' => function ($model, $key, $index, $widget) {
//            return $this->render('list/'.$model->type.'.php', ['model' => $model, 'index' => $index]);
//       },
                'itemView' => '_item',
                'pager' => ['class' => \kop\y2sp\ScrollPager::className(),
                    'triggerTemplate' => '<div class="show-more text-center"><a href="#lastnews"></a><div>{text}</div>'],
                'layout'=>"{items} {pager}",
            ]); ?>
        </div>
    </div>
</div>
