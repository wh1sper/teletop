<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\game */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-view">

    <img style='max-height:300px;' class="card-img-top" src="<?= $model->getImage(); ?>"/>

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= $model->descr ?></p>
    <p><a href="<?= $model->link ?>">Скачать</a></p>

</div>