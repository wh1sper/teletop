<div class="teletop_main_category">
    <ul>
        <li <? if($active == 'all'):?> class="active" <? endif; ?>><a href="/game">Все</a></li>
        <? foreach(\common\models\Game::$categories as $category=>$name): ?>
            <li <? if($active == $category): ?> class="active" <? endif; ?>><a href="/game/category/<?= $category ?>"><?= $name ?></a></li>
        <? endforeach; ?>
    </ul>
</div>