<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<section class="teletop_screen">
    <div class="teletop_header">
        <div class="teletop_header_logo">
                <span class="mobile_panel_left">
                <span></span>
                <span></span>
                <span></span>
                </span>
            <a href="/" class="logo"></a>
        </div>
        <div class="teletop_header_menu">
            <ul>
                <li class="teletop_header_menu_user"><span class="icon" style="background-image: url(img/user_foto.png)"></span><span class="text"><?= Yii::$app->user->identity->email ?></span></li>
                <li class="teletop_header_menu_logout"><a href="/site/logout"><span class="icon"></span><span class="text">Выход</span></a></li>
            </ul>
        </div>
    </div>
    <div class="teletop_body">
        <?= $content ?>
        <div class="teletop_left_panel slow">
            <div class="teletop_left_panel_group <? if(Yii::$app->controller->id == 'lenta' || (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id !== 'get')) echo "active"; ?>">
                <a href="/" class="teletop_left_panel_group_tittle">Лента</a>
            </div>
            <div class="teletop_left_panel_group <? if(Yii::$app->controller->id == 'top') echo "active"; ?>">
                <a href="/top" class="teletop_left_panel_group_tittle">В тренде</a>
            </div>
            <div class="teletop_left_panel_group <? if(Yii::$app->controller->id == 'channel') echo "active"; ?>">
                <a href="/channels" class="teletop_left_panel_group_tittle">Каналы</a>
            </div>
<!--            <div class="teletop_left_panel_group --><?// if(Yii::$app->controller->id == 'stickerpack') echo "active"; ?><!--">-->
<!--                <a href="/stickers" class="teletop_left_panel_group_tittle">Стикеры</a>-->
<!--            </div>-->
<!--            <div class="teletop_left_panel_group --><?// if(Yii::$app->controller->id == 'game') echo "active"; ?><!--">-->
<!--                <a href="/games" class="teletop_left_panel_group_tittle">Игры</a>-->
<!--            </div>-->
            <div class="teletop_left_panel_group <? if(Yii::$app->controller->action->id == 'get') echo "active"; ?>">
                <a href="/get" class="teletop_left_panel_group_tittle">Скачать Telegram</a>
            </div>
        </div>
        <? if(Yii::$app->controller->action->id != 'get' && Yii::$app->controller->id != "channel" && Yii::$app->controller->id != "category" && Yii::$app->controller->id != "top"): ?>
            <div class="teletop_main_right_panel slow">
                <span class="add_chanel slow"></span>
                <div class="teletop_main_right_panel_tittle">Добавить канал</div>
                <div class="teletop_main_right_panel_add">
                    <div class="add-channel-form">
                        <input id="quick-add" type="text">
                        <button id="quick-add-btn"></button>
                        <div class="row" id="added_info"></div>
                    </div>
                </div>
                <div class="teletop_main_right_panel_list">
                    <div class="teletop_main_right_panel_tittle">Мои каналы:</div>
                    <div class="chanel_list">
                        <ul>
                            <? foreach($this->params['channels'] as $ch): ?>
                                <li><?= $ch ?><button data-id="<?= $ch ?>" class="delete confirm-del-channel"></button></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="teletop_body_panel_bg slow"></div>
    </div>
</section>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
