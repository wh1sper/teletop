<?php

/* @var $this yii\web\View */
use yii\widgets\ListView;
use yii\jui\AutoComplete;

$this->title = 'Teletop';
?>



<div class="teletop_main teletop_main__lenta">
    <div class="teletop_main_lenta">
        <? if (!$dataProvider->totalCount || $empty_lenta) : ?>
            Вы не подписаны ни на один канал
        <? else: ?>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                // 'options' => ['id' => $type,'class' => 'row list-view '.mb_strtolower(Post::$types[$type]['eng'])],
                //        'itemOptions' => ['class' => 'item '.$class],
                //        'itemView' => function ($model, $key, $index, $widget) {
                //            return $this->render('list/'.$model->type.'.php', ['model' => $model, 'index' => $index]);
                //       },
                'itemView' => '/message/_item',
                'pager' => ['class' => \kop\y2sp\ScrollPager::className(),
                'triggerTemplate' => '<div class="show-more text-center"><a href="#lastnews"></a><div>{text}</div>'],
                'layout'=>"{items} {pager}",
            ]); ?>
        <? endif; ?>
    </div>
</div>

