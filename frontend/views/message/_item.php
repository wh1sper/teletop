<div class="teletop_main_lenta_post message" data-channel-id="<?= $model->channel_id ?>">
    <div class="teletop_main_lenta_post_inner">
        <div class="teletop_main_lenta_post_inner_top">
            <a href="https://telegram.me/<?= $model->channel->id ?>" target="" class="teletop_main_lenta_post_chanel"><span class="icon" style="background-image: url(/media/img/<?= $model->channel->image ?>)"></span><?= $model->channel->id ?></a>
        </div>
        <div class="message-text">
            <?= $this->render("content/".$model->getContent(), ["model"=>$model]); ?>
        </div>
        <div class="teletop_main_lenta_post_bottom">
            <div class="teletop_main_lenta_post_bottom_views"><?= $model->getViews(); ?></div>
            <div class="teletop_main_lenta_post_bottom_date"><?= $model->getDate(); ?></div>
        </div>
    </div>
</div>