<div class="message-img">
<? echo branchonline\lightbox\Lightbox::widget([
    'files' => [
        [
            'thumb' => WEB_DOWNLOAD_PATH.$model->media->src,
            'original' => WEB_DOWNLOAD_PATH.$model->media->src,
        ],
    ]
]);
?>
</div>

<div>
    <?
    $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
    $text = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $model->media->caption);
    echo nl2br($text);
    ?>
</div>