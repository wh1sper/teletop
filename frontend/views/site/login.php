<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use rmrevin\yii\ulogin\ULogin;


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Teletop';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="auth_screen">
    <div class="auth_screen_main">
        <div class="auth_screen_main_inner">
            <div class="auth_screen_main_logo_top">
                <div class="logo"></div>
                <div class="auth_screen_main_logo_tittle">Лента каналов Telegram на одном экране</div>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <label class="bb"><input type="email" name="LoginForm[email]" placeholder="E-mail"></label>
                <label class="bb bb_password"><input type="password" name="LoginForm[password]" placeholder="*****"></label>
                <label class="checkbox"><input type="checkbox"><span>Запомнить меня</span></label>
                <a href="#" class="button button_blue registration_popup_opener">Регистрация</a>
                <?= Html::submitButton('Вход', ['class' => 'button button_transparent', 'name' => 'login-button']) ?>
                <? if($errors = $model->getErrors()): ?>
                    <? if(isset($errors['password'])): ?>
                        <p style="color:red; padding-top:20px; font-size:15px;">Не правильный логин или пароль</p>
                    <? endif; ?>
                <? endif; ?>

            <?php ActiveForm::end(); ?>
            <div style="padding-top:30px">
                <? echo ULogin::widget([
                // widget look'n'feel
                'display' => ULogin::D_PANEL,

                // required fields
                'fields' => [ULogin::F_EMAIL],

                // optional fields
                'optional' => [ULogin::F_BDATE],

                // login providers
                'providers' => [ULogin::P_VKONTAKTE, ULogin::P_FACEBOOK, ULogin::P_TWITTER, ULogin::P_GOOGLE],

                // login providers that are shown when user clicks on additonal providers button
                'hidden' => [],

                // where to should ULogin redirect users after successful login
                'redirectUri' => ['site/ulogin-auth'],

                // optional params (can be ommited)
                // force widget language (autodetect by default)
                'language' => ULogin::L_RU,

                // providers sorting ('relevant' by default)
                'sortProviders' => ULogin::S_RELEVANT,

                // verify users' email (disabled by default)
                'verifyEmail' => '0',

                // mobile buttons style (enabled by default)
                'mobileButtons' => '1',
                ]); ?>
            </div>
        </div>
    </div>

    <div class="auth_screen_footer">
        <div class="auth_screen_footer_copyright">© Teletop 2017–2018</div>
    </div>
</section>

<div class="registration_popup" <? if(isset($show_reg)): ?>style="display:block;"<? endif; ?>>
    <span class="popup_close"></span>
    <div class="registration_popup_tittle">Регистрация</div>
    <form action="/" autocomplete="off" method="post">
        <label class="bb"><input name="Signup[email]" type="email" placeholder="Email"></label>
        <label class="bb"><input name="Signup[password]" type="password" placeholder="Пароль"></label>
        <button class="button button_blue">Регистрация</button>
        <? if($errors = $model->getErrors()): ?>
            <? if(isset($errors['email'])): ?>
                <p style="color:red; padding-top:20px; font-size:15px;">Этот e-mail занят</p>
            <? endif; ?>
        <? endif; ?>
    </form>
</div>
<div class="registration_popup_bg"  <? if(isset($show_reg)): ?>style="display:block;"<? endif; ?>></div>
