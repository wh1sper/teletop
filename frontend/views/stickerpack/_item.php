<a href="/stickerpack/<?= $model->id ?>" class="teletop_main_item">
    <span class="teletop_main_item_icon" style="background-image: url(<?= $model->getImage(); ?>)"></span>
    <span class="teletop_main_item_name"><?= $model->name ?></span>
</a>