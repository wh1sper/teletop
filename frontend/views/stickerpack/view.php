<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Stickerpack */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Stickerpacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stickerpack-view">
    <h1><?= $model->name ?></h1>
    <p><a href="<?= $model->link ?>" target="_blank">Ссылка</a></p>
    <div class="stickers row">
        <? if($model->stickers): ?>
            <? foreach($model->stickers as $sticker): ?>
                <?= $this->render("/sticker/_item", ['model' => $sticker]); ?>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</div>
