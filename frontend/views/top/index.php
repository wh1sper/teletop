

<div class="teletop_main teletop_main__lenta">
    <div class="teletop_main_lenta">
        <div class="col-xs-6">
            <h1 style="padding-bottom:30px;">Лучшее за час</h1>
            <? $i=1; ?>
            <? foreach($hourly as $views=>$messages): ?>
                <? foreach($messages as $k=>$model): ?>
                    <? if($i<11): ?>
                        <?= $this->render("/message/_item",['model'=>$model,'views'=>$views,'pos'=>$i]); ?>
                        <? $i++; ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>

        <div class="col-xs-6 pull-right">
            <h1 style="padding-bottom:30px;">Лучшее за день</h1>
            <? $i=1; ?>
            <? foreach($daily as $views=>$messages): ?>
                <? foreach($messages as $k=>$model): ?>
                    <? if($i<11): ?>
                        <?= $this->render("/message/_item",['model'=>$model,'views'=>$views,'pos'=>$i]); ?>
                        <? $i++; ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>

