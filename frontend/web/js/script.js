$(document).ready(function() {

});

$("body").on('click',".add-channel",function(e) {
    e.preventDefault();
    $self = $(this);
    $id = $(this).data('id');
    $.post("/channel/lenta", { channel_id : $id }, function(answer) {
         if(answer == 1) {
             $self.text('Отписаться');
             $self.removeClass('button_green');
             $self.addClass('button_red');
         }
         if(answer == 0) {
            // $(".message[data-channel-id='"+$id+"']").remove();
             $self.text('Подписаться');
             $self.removeClass('button_red');
             $self.addClass('button_green');
         }
    });
});

$("body").on('click','#quick-add-btn',function(e) {
    $.post("/channel/add", { id: $("#quick-add").val() }, function(data) {
        if(parseInt(data.success) == 1) {
            if(data.added == 1) {
                $("#added_info").text("Канал добавлен в систему. После модерации он будет добавлен в вашу ленту.").show();
            }
            else {
                location.reload();
            }
        } else {
            $("#added_info").text("Вы указали неверный канал.").show();
        }
    },"json");
});

$("body").on('click',".confirm-del-channel",function(e) {
    e.preventDefault();
    $id = $(this).data('id');
    if(confirm("Удалить канал из ленты?")) {
        $.post("/channel/lenta", {channel_id: $id}, function (answer) {
            location.reload();
        });
    }
});