$(document).ready(function () {
    $('.registration_popup_opener').on('click', function () {
        $('.registration_popup, .registration_popup_bg').fadeIn();
    });

    $('.popup_close, .registration_popup_bg').on('click', function () {
        $('.registration_popup, .registration_popup_bg').fadeOut();
    });

    $('.teletop_left_panel_group_inner_more').on('click', function () {
        $(this).parent('.teletop_left_panel_group_inner').addClass('opened');
        $(this).parent().children().children('li').slideDown();
        $(this).slideUp();
    });

    $('.mobile_panel_left').on('click', function () {
        $('.teletop_left_panel, .teletop_body_panel_bg').toggleClass('opened');
        $('.teletop_main_right_panel, .add_chanel').removeClass('opened');
    });

    $('.teletop_body_panel_bg').on('click', function () {
        $('.teletop_left_panel, .teletop_body_panel_bg').removeClass('opened');
    });

    $('.add_chanel').on('click', function () {
        $(this).toggleClass('opened');
        $('.teletop_main_right_panel').toggleClass('opened');
        $('.teletop_left_panel, .teletop_body_panel_bg').removeClass('opened');
    });


    $(".teletop_main_category").mCustomScrollbar({
        axis: "x",
        scrollButtons: {
            enable: true,     
            scrollAmount: 10
        },
    });

});